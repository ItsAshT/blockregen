package xyz.itsashdev.blockregen;


import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import xyz.itsashdev.blockregen.listeners.BlockBreak;
import xyz.itsashdev.blockregen.listeners.BlockDamage;
import xyz.itsashdev.blockregen.listeners.BlockExp;
import xyz.itsashdev.blockregen.listeners.PlayerJoin;

public final class BlockRegen extends JavaPlugin {

  @Override
  public void onEnable() {
    // Register events
    Bukkit.getPluginManager().registerEvents(new BlockBreak(), this);
    Bukkit.getPluginManager().registerEvents(new PlayerJoin(), this);
    Bukkit.getPluginManager().registerEvents(new BlockDamage(), this);
    Bukkit.getPluginManager().registerEvents(new BlockExp(), this);

    // Save default config
    getConfig().options().copyDefaults(true);
    saveDefaultConfig();
  }

  @Override
  public void onDisable() {
    saveConfig();
  }

  public static Plugin getInstance() {
    return BlockRegen.getPlugin(BlockRegen.class);
  }
}
