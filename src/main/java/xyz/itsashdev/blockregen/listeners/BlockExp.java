package xyz.itsashdev.blockregen.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockExpEvent;

public class BlockExp implements Listener {

  @EventHandler
  public void onBlockExp(BlockExpEvent event) {
    event.setExpToDrop(0);
  }
}
