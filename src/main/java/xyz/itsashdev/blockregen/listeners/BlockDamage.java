package xyz.itsashdev.blockregen.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDamageEvent;

public class BlockDamage implements Listener {

  @EventHandler
  public void onBlockDamage(BlockDamageEvent event) {
    Player player = event.getPlayer();

    // If player has empty had, cancel event
    if (event.getItemInHand().getItemMeta() == null) {
      event.setCancelled(true);
      return;
    }

    event
        .getItemInHand()
        .getItemMeta()
        .getDestroyableKeys()
        .forEach(
            key -> {
              if (!event.getBlock().getType().getKey().equals(key)) {
                event.setCancelled(true);
                player.sendMessage("You can't break this block with this tool!");
              }
            });
  }
}
