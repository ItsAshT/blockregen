package xyz.itsashdev.blockregen.listeners;

import com.destroystokyo.paper.Namespaced;
import java.util.Collection;
import java.util.HashSet;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class PlayerJoin implements Listener {

  @EventHandler
  public void onPlayerJoin(PlayerJoinEvent event) {
    Player player = event.getPlayer();
    if (player.hasPlayedBefore()) {
      ItemStack wooden_pickaxe = new ItemStack(Material.WOODEN_PICKAXE);
      ItemMeta meta = wooden_pickaxe.getItemMeta();
      Collection<Namespaced> destroyableKeys = new HashSet<>();
      destroyableKeys.add(new NamespacedKey("minecraft", "coal_ore"));
      meta.setDestroyableKeys(destroyableKeys);
      meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
      meta.isUnbreakable();
      wooden_pickaxe.setItemMeta(meta);
      player.getInventory().addItem(wooden_pickaxe);
    }
  }
}
