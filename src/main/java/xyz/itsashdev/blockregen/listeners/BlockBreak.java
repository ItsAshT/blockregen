package xyz.itsashdev.blockregen.listeners;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;
import xyz.itsashdev.blockregen.BlockRegen;

public class BlockBreak implements Listener {

  @EventHandler
  public void onBlockBreak(@NotNull BlockBreakEvent event) {

    if (event.getPlayer().hasPermission("blockregen.godmode")) {
      event.setCancelled(false);
      return;
    }

    Material drop =
        switch (event.getBlock().getType()) {
          case COAL_ORE -> Material.COAL;
          case IRON_ORE -> Material.IRON_INGOT;
          default -> Material.AIR;
        };

    int timer =
        switch (event.getBlock().getType()) {
          case COAL_ORE, IRON_ORE -> 5;
          default -> 0;
        };

    String blockName = event.getBlock().getType().toString();
    event.setCancelled(true);
    ItemStack blockItem = new ItemStack(drop);
    event.getPlayer().getInventory().addItem(blockItem);
    event.getBlock().setType(Material.STONE);
    BlockRegen.getInstance()
        .getServer()
        .getScheduler()
        .runTaskLater(
            BlockRegen.getInstance(),
            () -> event.getBlock().setType(Material.valueOf(blockName)),
            timer * 20L);
  }
}
